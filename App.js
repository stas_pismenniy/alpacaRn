import * as React from 'react'
import { Provider } from 'react-redux'
import AppNavigator from './src/navigation/AppNavigation'
import { PersistGate } from 'redux-persist/integration/react'
import { persisStore, store } from './src/store'
import { SafeAreaView } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import {linking} from './src/navigation/Linking'

export default function App() {
  return (
    <SafeAreaView style={{flex: 1}}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persisStore}>
          <NavigationContainer linking={linking}>
            <AppNavigator />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    </SafeAreaView>
  )
}
