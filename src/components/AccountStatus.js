import {Text, View} from 'react-native'
import {accountStyle} from '../styles/styles'
import React from 'react'

export const AccountStatus = ({data}) => {
  const {
    equity,
    buying_power,
    cash,
    portfolio_value
  } = data
  return (
    <View style={accountStyle.account}>
      <Text style={accountStyle.heading}>Account</Text>

      <View style={accountStyle.accountCell}>
        <View style={accountStyle.leftSide}>
          <Text style={accountStyle.label}>Equity</Text>
          <Text>${equity}</Text>
          <Text style={accountStyle.label}>Buying Power</Text>
          <Text>${buying_power}</Text>
        </View>

        <View style={accountStyle.rightSide}>
          <Text style={accountStyle.label}>Portfolio Value</Text>
          <Text>${portfolio_value}</Text>
          <Text style={accountStyle.label}>Cash</Text>
          <Text>${cash}</Text>
        </View>

      </View>
    </View>
  )
}
