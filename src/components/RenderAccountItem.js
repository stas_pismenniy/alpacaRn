import {Text, View} from 'react-native'
import {accountStyle} from '../styles/styles'
import React from 'react'

export const RenderAccountItem = ({item}) => {
  const {
    asset_id,
    symbol,
    current_price,
    qty,
    avg_entry_price,
    change_today
  } = item
  return (
    <View key={asset_id} style={accountStyle.positions}>
      <View style={accountStyle.positionsLeftCell}>
        <Text style={accountStyle.symbol}>{symbol}</Text>
        <Text style={accountStyle.subHead}>{qty} @ {avg_entry_price}</Text>
      </View>
      <View style={accountStyle.positionsRightCell}>
        <Text style={accountStyle.price}>
          {current_price}
        </Text>
        <Text>{(change_today * 100).toFixed(2)}</Text>
      </View>
    </View>
  )
}
