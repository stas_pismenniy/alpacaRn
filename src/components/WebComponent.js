import React from 'react'
import {View, StyleSheet} from 'react-native'
import { WebView } from 'react-native-webview'
const WebComponent = ({ url }) => {
  return (
    <View style={styles.container}>
    <WebView
      source={{uri: url}}
    />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
})

export default WebComponent

