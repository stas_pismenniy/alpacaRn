import React from 'react'
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native'

export const AlpacaConnectButton = ({getAccessToken}) => {
  const handleGetAccessToken = () => {
    return getAccessToken()
  }
  return (
    <TouchableOpacity style={styles.container} onPress={handleGetAccessToken}>
      <View>
        <Text>Connect to Alpaca</Text>
      </View>
    </TouchableOpacity>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
