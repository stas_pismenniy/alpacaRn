import config from '../config'
import axios from 'axios'
const http = axios.create({
  baseURL: `${config.BASE_URL}`
})

export default {
  get (url, accessToken) {
    return http.get(url, {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      },
    })
  },
  post (url, data) {
    return http.post(url, data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
}

