import * as React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { GainLoss } from '../screens/GainLoss'
import { Search } from '../screens/Search'
import { LogoUt } from '../screens/LogoUt'
import { MainScreen } from '../screens/MainScreen'

const Tab = createBottomTabNavigator()

const MainTabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name='MainScreen' component={MainScreen} />
      <Tab.Screen name='GainLoss' component={GainLoss} />
      <Tab.Screen name='Search' component={Search} />
      <Tab.Screen name='LogoUt' component={LogoUt} />
    </Tab.Navigator>
  )
}

export default MainTabs
