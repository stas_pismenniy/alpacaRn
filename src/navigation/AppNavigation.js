// import React from 'react'
// import {createAppContainer, createSwitchNavigator} from 'react-navigation'
// import { createBottomTabNavigator } from 'react-navigation-tabs'
// import {createStackNavigator} from 'react-navigation-stack'
// import { MainScreen } from '../screens/MainScreen'
// import { LoginScreen } from '../screens/LoginScreen'
// import { Splash } from '../screens/Splash'
// import { GainLoss } from '../screens/GainLoss'
// import { Search } from '../screens/Search'
// import { LogoUt } from '../screens/LogoUt'
//
// const AppStack = createBottomTabNavigator({
//   Main: {
//     screen: MainScreen,
//     path: 'main',
//     navigationOptions: {
//       headerTitle: 'Account',
//       title: 'Account'
//     }
//   },
//   GainLoss: {
//     screen: GainLoss
//   },
//   Search: {
//     screen: Search
//   },
//   LogoUt: {
//     screen: LogoUt
//   }
// })
// const AuthStack = createStackNavigator({
//   LoginScreen: {
//     screen: LoginScreen,
//     navigationOptions: {
//       headerTitle: 'Login'
//     },
//     path: 'login'
//   }
// })
//
// const AppContainer = createAppContainer(
//   createSwitchNavigator({
//       AuthLoading: {
//         screen: Splash,
//       },
//       App: {
//         screen: AppStack,
//       },
//       Auth: AuthStack
//   },
//     {
//       initialRouteName: 'AuthLoading'
//     }
//   )
// )
// export default () => {
//   const prefix = 'alpaca://'
//   return <AppContainer uriPrefix={prefix} />
// }

import React, { useState, useEffect} from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Splash } from '../screens/Splash'
import MainTabs from './MainTabs'
import AuthTabs from './AuthTabs'
import { useSelector } from 'react-redux'

const Stack = createStackNavigator()

const AppNavigator = () => {
  // Check if user has AccessToken to do a request calls
  const user = useSelector(state => state && state.user && state.user.accessToken)
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    setTimeout(() => {
      setLoading(!loading)
    }, 2000)
  }, [])
  if (loading) {
    // We haven't finished checking for the token yet
    return <Splash />
  }
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {
        user ? <Stack.Screen name='MainTabs' component={MainTabs} /> :
          <Stack.Screen name='AuthTabs' component={AuthTabs} />
      }
    </Stack.Navigator>
  )
}

export default AppNavigator
