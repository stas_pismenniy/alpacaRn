const config = {
  screens: {
    MainTabs: {
      screens: {
        MainScreen: {
          path: 'main'
        },
        GainLoss: {
          path: 'gain'
        },
        Search: {
          path: 'search'
        },
        LogoUt: {
          path: 'logout'
        }
      }
    }
  }
}

export const linking = {
  prefixes: ['alpaca://'],
  config
}
