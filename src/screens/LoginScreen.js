import React from 'react'
import {View, StyleSheet} from 'react-native'
import { getAccessTokenAction } from '../store/actions'
import { AlpacaConnectButton } from '../components/AlpacaConnectButton'
import {connectToAlpaca} from '../utils/connectToAlpaca'
import {useDispatch} from 'react-redux'

export const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch()
  // Get AccessToken from the Api
  async function getAccessToken () {
    await connectToAlpaca().then(async data => {
      const accessToken = await data.data
      await dispatch(getAccessTokenAction(accessToken))
      // navigation.navigate('MainTabs', {screen: 'MainScreen'})
    }).catch(e => console.log('errorGetAccessToken', e))
  }
  let content = (<AlpacaConnectButton getAccessToken={getAccessToken} />)
  return (
    <View style={styles.center}>
      {content}
    </View>
  )
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    backgroundColor: '#fff'
  }
})
