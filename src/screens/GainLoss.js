import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { View, Text } from 'react-native'

export const GainLoss = () => {
  const { equity, last_equity } = useSelector(state => state.account.data)
  useEffect(() => {
  },[])
  const balanceChange = (equity - last_equity).toFixed(2)
  return (
    <View style={styles.container}>
      <Text>Gain/Loss</Text>
      <Text>{balanceChange}</Text>
    </View>
  )
}

import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

