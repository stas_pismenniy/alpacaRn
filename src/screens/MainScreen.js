import React, {useState, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { View, Text, FlatList, ActivityIndicator } from 'react-native'
import api from '../services/api'
import { getAccount } from '../store/actions'
import { accountStyle } from '../styles/styles'
import { RenderAccountItem } from '../components/RenderAccountItem'
import { AccountStatus } from '../components/AccountStatus'

export const MainScreen = () => {
  const [positions, setPositions] = useState([])
  const [indicator, setIndicator] = useState(true)
  const accessToken = useSelector(state => state.user.accessToken)
  const data = useSelector(state => state.account.data)
  console.log('data123', data)
  const dispatch = useDispatch()
  // Get Account User Overview Data from the Api
  async function getData () {
    try {
      // await api.get('/account', accessToken).then(async account => {
      //   console.log('account', account)
      //   // setData(account.data)
      //   await dispatch(getAccount(account.data))
      // }).catch(e => console.log('error', e))
      setIndicator(false)
      await dispatch(getAccount())
      await api.get('/positions', accessToken).then(async positions => {
        await console.log('positions', positions.data)
        setPositions(positions.data)
      }).catch(e => console.log('error', e))
    }
    catch (error) {
      console.log('errorGetData')
      setIndicator(false)
    }
  }

  useEffect(() => {
    const interval = setInterval(async () => {
      await getData()
    }, 5000)
    return () => clearInterval(interval)
  },[])

  return (
    <>
      {indicator && (
        <View style={{
          backgroundColor: 'rgba(0,0,0,0.5)',
          opacity: 1,
          justifyContent: 'center',
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          top: 0
        }}>
          <ActivityIndicator />
        </View>
      )
      }
      <View style={accountStyle.container}>
        <AccountStatus data={data}/>
        <View style={{flex: 5}}>
          <Text style={accountStyle.heading}>Positions</Text>
          <FlatList
            data={positions}
            renderItem={RenderAccountItem}
            keyExtractor={item => item.asset_id}
          />
        </View>

      </View>
    </>
  )
}
