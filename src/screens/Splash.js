import React, { useEffect } from 'react'
import {useSelector} from 'react-redux'
import {View, StyleSheet, Text, Image} from 'react-native'
import images from '../utils/images.util'
export const Splash = (props) => {
  console.log('Splash props', props)
  return (
    <View style={styles.center}>
      <Image
        style={styles.logo}
        source={images.logo}
        resizeMode="contain"
      />
    </View>
  )
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  logo: {
    width: 300,
    height: 300
  }
})
