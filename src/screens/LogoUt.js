import React  from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { userLoggedOut } from '../store/actions'
import { useDispatch } from 'react-redux'

export const LogoUt = ({navigation}) => {
  const dispatch = useDispatch()
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => {
        dispatch(userLoggedOut())
        // navigation.navigate('AuthTabs', {screen: 'LoginScreen'})
      }}>
        <Text>Press LogoUt</Text>
      </TouchableOpacity>
    </View>
  )
}

import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
