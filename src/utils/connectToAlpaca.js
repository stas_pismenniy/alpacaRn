import {authorize} from 'react-native-app-auth'
import api from '../services/api'
import config from '../config'
const client_id = config.APCA_API_KEY_ID
const client_secret = config.APCA_API_SECRET_KEY
const authorization_endpoint = config.AUTHORIZATION_ENDPOINT
const redirect_uri = 'alpaca://login'

const configParam = {
  issuer: authorization_endpoint,
  clientId: client_id,
  redirectUrl: redirect_uri,
  skipCodeExchange: true,
  serviceConfiguration: {
    authorizationEndpoint: authorization_endpoint,
    tokenEndpoint: config.TOKEN_ENDPOINT
  },
}

export const connectToAlpaca = async function connectToAlpaca() {
  try {
    const result = await authorize(configParam)
    const data = `grant_type=authorization_code&code=${result.authorizationCode}&client_id=${client_id}&client_secret=${client_secret}&redirect_uri=${redirect_uri}`
    return await api.post(config.TOKEN_ENDPOINT, data)
  } catch (error) {
    console.log('error', error)
  }
}
