import {
  GET_ACCOUNT,
  USER_LOGGED_OUT,
  ERROR
} from './types'

import api from '../../services/api'

export const getAccount = () => async (dispatch, getState) => {
  const token = getState().user.accessToken
  await api.get('/account', token).then(data => {
    dispatch({
      type: GET_ACCOUNT,
      payload: data.data
    })
  }).catch(e => {
    dispatch({
      type: ERROR,
      payload: e.response.data.message
    })
  })
}

export const userLoggedOut = () => async dispatch => {
  dispatch({
    type: USER_LOGGED_OUT
  })
}
