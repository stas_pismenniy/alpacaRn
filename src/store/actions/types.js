export const GET_ACCESS_TOKEN = 'GET_ACCESS_TOKEN'
export const GET_ACCOUNT = 'GET_ACCOUNT'
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT'
export const ERROR = 'ERROR'
