import {
  GET_ACCESS_TOKEN
} from './types'

export const getAccessTokenAction = accessToken => async dispatch => {
  console.log(accessToken, 'accessToken')
  dispatch({
    type: GET_ACCESS_TOKEN,
    payload: accessToken
  })
  // return {
  //   type: GET_ACCESS_TOKEN,
  //   payload: accessToken
  // }
}

