import {
  GET_ACCOUNT,
  USER_LOGGED_OUT,
  ERROR
} from '../actions/types'

const initialState = {
  data: [],
  error: null
}

export default (state = initialState, action) => {
  console.log('action.payload', action)
  switch (action.type) {
    case GET_ACCOUNT:
      return {...state, data: action && action.payload}
    case ERROR:
      return {...state, error: action.payload}
    case USER_LOGGED_OUT:
      return {...state, data: []}
    default:
      return state
  }
}
