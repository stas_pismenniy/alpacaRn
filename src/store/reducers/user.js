import {
  GET_ACCESS_TOKEN,
  USER_LOGGED_OUT
} from '../actions/types'

const initialState = {
  user: [],
  accessToken: null
}

export default (state = initialState, action) => {
  console.log('action.payload', action)
  switch (action.type) {
    case GET_ACCESS_TOKEN:
      return {...state, accessToken: action && action.payload.access_token}
      case USER_LOGGED_OUT:
      return {...state, accessToken: null}
    default:
      return state
  }
}
