import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import reducers from './reducers'
const rootReducer = reducers

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
let composeEnhancers = compose

export const store = createStore(
  persistedReducer,
  {},
  composeEnhancers(
    applyMiddleware(thunk)
  )
)
export const persisStore = persistStore(store)
export default store
