import { StyleSheet } from 'react-native'

export const accountStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: 15,
    marginTop: 50
  },
  account: {
    flex: 3
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 10
  },
  accountCell: {
    flex: 1,
    flexDirection: 'row'
  },
  leftSide: {
    flex: 1
  },
  rightSide: {
    flex: 1
  },
  positions: {
    flex: 1,
    flexDirection: 'row',
    margin: 5,
    borderWidth: 1,
    padding: 5,
    borderRadius: 5
  },
  positionsLeftCell: {
    flex: 4
  },
  positionsRightCell: {
    flex: 1
  },
  symbol: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  subHead: {
    color: '#808080'
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'green'
  }
})
